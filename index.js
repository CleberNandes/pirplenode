/* primary file for the api */

// dependencies

var http = require('http');
// import * as http from 'http';
var url = require('url');
var StringDecoder = require('string_decoder').StringDecoder;

// the server should respond to all requests with a string
var server = http.createServer((req, res) => {

  // get the url and parse it
  var parseUrl = url.parse(req.url, true);
  // get the path
  var path = parseUrl.pathname;
  var trimmedPath = path.replace(/^\/+|\/+$/g,'');
  // Get the query string as an object
  var queryStringObject = parseUrl.query;
  // get the http method
  var method = req.method.toLowerCase();
  // get the headers as an object
  var headers = req.headers;
  // get the payload, if any
  var decoder = new StringDecoder('utf-8');
  var buffer = '';
  req.on('data', (data) => {
    buffer += decoder.write(data);
  });
  req.on('end', () => {
    buffer += decoder.end();
    // send the response
    res.end('hello world\n');
    // log the request path
    console.log('Buffer: '+ buffer);
    
    console.log('Request received on on path: '+ trimmedPath);
    console.log('method: '+ method);
    console.log('parseUrl: ' + parseUrl);
    console.log('headers: ' + headers);
  });

  
});
// start the server, and have it listen on port 3000
server.listen(3000, () => {
 console.log('the serves is listening on port 3000 now');
});

